var config = {
	map: {
		'*': {
			jquerybootstrap: "Sm_Paco/js/bootstrap/bootstrap.min",
			owlcarousel: "Sm_Paco/js/owl.carousel",
			jquerycookie: "Sm_Paco/js/jquery.cookie.min",
			jqueryfancyboxpack: "Sm_Paco/js/jquery.fancybox.pack",	
			yttheme: "Sm_Paco/js/yttheme"			
		}
	}
};