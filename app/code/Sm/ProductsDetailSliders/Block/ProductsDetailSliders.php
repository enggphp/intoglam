<?php
/**------------------------------------------------------------------------
* SM Products Detail Sliders - Version 1.1.0
* Copyright (c) 2015 YouTech Company. All Rights Reserved.
* @license - Copyrighted Commercial Software
* Author: YouTech Company
* Websites: http://www.magentech.com
-------------------------------------------------------------------------*/

namespace Sm\ProductsDetailSliders\Block;

class ProductsDetailSliders extends \Magento\Catalog\Block\Product\AbstractProduct
{
	protected $_config = null;

	/**
	 * Currently selected store ID if applicable
	 *
	 * @var int
	 */
	protected $_storeId;

	/**
	 * @var \Magento\Framework\App\Config\ScopeConfigInterface
	 */
	protected $_scopeConfigInterface;

	/**
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $_storeManager;

	/**
	 * @var \Magento\Framework\Filesystem
	 */
	protected $_directory;

	/**
	 * Object manager
	 *
	 * @var \Magento\Framework\ObjectManagerInterface
	 */
	private $_objectManager;

	/**
	 * @var \Magento\Eav\Model\Config
	 */
	protected $_eavConfig;

	/**
	 * Resource
	 *
	 * @var \Magento\Framework\App\ResourceConnection
	 */
	protected $_resource;

	/**
	 * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
	 */
	protected $localeDate;

	/**
	 * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
	 */
	protected $_configurableType;

	/**
	 * @var \Magento\Framework\Pricing\PriceCurrencyInterface
	 */
	protected $priceCurrency;

	/**
	 * Class constructor
	 *
	 * @param \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableType
	 * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
	 * @param \Magento\Framework\App\ResourceConnection $resourceConnection
	 * @param \Magento\Framework\ObjectManagerInterface $objectManager
	 * @param \Magento\Catalog\Block\Product\Context $context
	 * @param \Magento\Eav\Model\Config $eavConfig
	 * @param array $data
	 * @param string|null $scope
	 */
	public function __construct(
		\Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableType,
		\Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
		\Magento\Framework\App\ResourceConnection $resourceConnection,
		\Magento\Framework\ObjectManagerInterface $objectManager,
		\Magento\Catalog\Block\Product\Context $context,
		\Magento\Eav\Model\Config $eavConfig,
		array $data = [],
		$attr = null
	)
	{
		$this->_eavConfig = $eavConfig;
		$this->_objectManager = $objectManager;
		$this->_storeManager = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		$this->_scopeConfigInterface = $this->_objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
		$this->_storeId = (int)$this->_storeManager->getStore()->getId();
		$this->priceCurrency = $priceCurrency;
		$this->_resource = $resourceConnection;
		$this->localeDate = $this->_objectManager->get('\Magento\Framework\Stdlib\DateTime\TimezoneInterface');
		$this->_directory = $this->_objectManager->get('\Magento\Framework\Filesystem');
		$this->_configurableType = $configurableType;
		$this->_config = $this->_getCfg($attr, $data);
		parent::__construct($context, $data);
	}

	public function _getCfg($attr = null , $data = null)
	{
		// get default config.xml
		$defaults = [];
		$collection = $this->_scopeConfigInterface->getValue('productsdetailsliders');

		if (empty($collection)) return;
		$groups = [];
		foreach ($collection as $def_key => $def_cfg) {
			$groups[] = $def_key;
			foreach ($def_cfg as $_def_key => $cfg) {
				$defaults[$_def_key] = $cfg;
			}
		}

		// get configs after change
		$_configs = $this->_scopeConfigInterface->getValue('productsdetailsliders');
		if (empty($_configs)) return;
		$cfgs = [];

		foreach ($groups as $group) {
			$_cfgs = $this->_scopeConfigInterface->getValue('productsdetailsliders/'.$group.'', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			foreach ($_cfgs as $_key => $_cfg) {
				$cfgs[$_key] = $_cfg;
			}
		}

		// get output config
		$configs = [];
		foreach ($defaults as $key => $def) {
			if (isset($defaults[$key])) {
				$configs[$key] = $cfgs[$key];
			} else {
				unset($cfgs[$key]);
			}
		}

		$cf = ($attr != null) ? array_merge($configs, $attr) : $configs;
		$this->_config = ($data != null) ? array_merge($cf, $data) : $cf;
		return $this->_config;
	}

	public function _getConfig($name = null, $value_def = null)
	{
		if (is_null($this->_config)) $this->_getCfg();
		if (!is_null($name)) {
			$value_def = isset($this->_config[$name]) ? $this->_config[$name] : $value_def;
			return $value_def;
		}
		return $this->_config;
	}

	public function _setConfig($name, $value = null)
	{

		if (is_null($this->_config)) $this->_getCfg();
		if (is_array($name)) {
			$this->_config = array_merge($this->_config, $name);
			return;
		}
		if (!empty($name) && isset($this->_config[$name])) {
			$this->_config[$name] = $value;
		}
		return true;
	}

	protected function _toHtml()
	{
		if (!$this->_getConfig('isenabled', 1)) return;

		$use_cache = (int)$this->_getConfig('use_cache');
		$cache_time = (int)$this->_getConfig('cache_time');
		$folder_cache = $this->_directory->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::CACHE)->getAbsolutePath();
		$folder_cache = $folder_cache.'Sm/ProductsDetailSliders/';
		if(!file_exists($folder_cache))
			mkdir ($folder_cache, 0777, true);

		$options = array(
			'cacheDir' => $folder_cache,
			'lifeTime' => $cache_time
		);
		$Cache_Lite = new \Sm\ProductsDetailSliders\Block\Cache\Lite($options);
		if ($use_cache){
			$hash = md5( serialize([$this->_getConfig(), $this->_storeId ,$this->_storeManager->getStore()->getCurrentCurrencyCode()]) );
			if ($data = $Cache_Lite->get($hash)) {
				return  $data;
			} else {
				$template_file = $this->getTemplate();
				$template_file = (!empty($template_file)) ? $template_file : "Sm_ProductsDetailSliders::default.phtml";
				$this->setTemplate($template_file);
				$data = parent::_toHtml();
				$Cache_Lite->save($data);
			}
		}else{
			if(file_exists($folder_cache))
				$Cache_Lite->_cleanDir($folder_cache);
			$template_file = $this->getTemplate();
			$template_file = (!empty($template_file)) ? $template_file : "Sm_ProductsDetailSliders::default.phtml";
			$this->setTemplate($template_file);
		}

		return parent::_toHtml();
	}

	public function _helper()
	{
		return $this->_objectManager->get('\Sm\ProductsDetailSliders\Helper\Data');
	}

	public function _getSelectSource(){
		$helper = $this->_helper();
		$image_config = [
			'width' => (int)$this->_getConfig('img_width', 600),
			'height' => $this->_getConfig('img_height', null),
			'background' => (string)$this->_getConfig('img_background'),
			'function' => (int)$this->_getConfig('img_function')
		];

		$image_thumb_config = [
			'width' => (int)$this->_getConfig('img_width_thumb', 200),
			'height' => $this->_getConfig('img_height_thumb', null),
			'background' => (string)$this->_getConfig('img_background'),
			'function' => (int)$this->_getConfig('img_function_thumb')
		];

		$product_source = $this->_getConfig('product_source');
		switch($product_source)
		{
			case 'catalog':
			case 'ids':
				if ($product_source == 'catalog')
					$products = $this->_getProductCatalog();
				else
					$products = $this->_getProductsIDs();

				if ($products != null)
				{
					$_products = $products->getItems();
					if (!empty($_products)) {
						foreach ($_products as $_product) {
							$_product->setStoreId($this->_storeId);
							$_product->title = $_product->getName();
							$image = $helper->getProductImage($_product, $this->_getConfig());
							$_image = $helper->_resizeImage($image, $image_config);
							$_image_thumb = $helper->_resizeImage($image, $image_thumb_config);
							$_product->_image = $_image;
							$_product->_image_thumb = $_image_thumb;
							$_product->_description = $helper->_cleanText($_product->getDescription());
							$_product->_description = $helper->_trimEncode($_product->_description != '') ? $_product->_description : $helper->_cleanText($_product->getShortDescription());
							$_product->_description = $helper->_trimEncode($_product->_description != '') ? $helper->truncate($_product->_description, $this->_getConfig('product_description_maxlength')) : '';
							$_product->link = $_product->getProductUrl();

							if ($_product->getAttributes())
								$_product->detailInfor = $this->getDetailInfor($_product);

							if($_product->getHasOptions())
								$_product->optionsConfigurable = $this->getOptionsProducts($_product);
						}
						return $_products;
					}
				}
				break;
		}
	}

	public function getDetailInfor($_product){
		$data = [];
		$excludeAttr = [];
		$attributes = $_product->getAttributes();
		foreach ($attributes as $attribute) {
			if ($attribute->getIsVisibleOnFront() && !in_array($attribute->getAttributeCode(), $excludeAttr)) {
				$value = $attribute->getFrontend()->getValue($_product);

				if (!$_product->hasData($attribute->getAttributeCode())) {
					$value = __('N/A');
				} elseif ((string)$value == '') {
					$value = __('No');
				} elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
					$value = $this->priceCurrency->convertAndFormat($value);
				}

				if (is_string($value) && strlen($value)) {
					$data[$attribute->getAttributeCode()] = [
						'label' => __($attribute->getStoreLabel()),
						'value' => $value,
						'code' => $attribute->getAttributeCode(),
					];
				}
			}
		}

		return $data;
	}

	public function getOptionsProducts($_product){
		$la = [];
		/*
		 * Get option of products configurable as array
		 * */
		$configurableAttributes = $this->_configurableType->getConfigurableAttributesAsArray($_product);
		if ($configurableAttributes != null)
		{
			foreach($configurableAttributes as $c){
				$arr = [];
				foreach($c['values'] as $ar)
				{
					$arr[] = $ar['store_label'];
				}
				$la[] = [
					'code'=>$c['attribute_code'],
					'label'=>$c['store_label'],
					'values'=>$arr
				];
			}
		}

		/*
		 * Get custom options of products
		 * */
		$opt = $this->_objectManager->create('Magento\Catalog\Model\Product')->load($_product->getId());
		if ($opt->getOptions() != null)
		{
			foreach($opt->getOptions() as $o)
			{
				$array = [];
				foreach($o->getValues() as $v)
				{
					$array[] = $v->getTitle();
				}
				$la[] = [
					'code'=>strtolower($o->getTitle()),
					'label'=>$o->getTitle(),
					'values'=>$array
				];
			}
		}

		return $la;
	}

	public function _getProductCatalog()
	{
		$catids = $this->_getConfig('product_category');
		$inlucde = (int)$this->_getConfig('child_category_products', 1);
		$level = (int)$this->_getConfig('max_depth', 1);
		if ($catids == null) return;
		$_catids = $this->_getCatActive($catids);
		$_catids = ($inlucde && $level > 0) ? $this->_childCategory($_catids, true, 0, (int)$level) : $_catids;
		if (empty($_catids)) return;
		$products = $this->_getProductsBasic($_catids);
		return $products;
	}

	private function _getCatActive($catids = null, $orderby = true)
	{
		if (is_null($catids)) {
			$catids = $this->_getConfig('product_category');
		}
		!is_array($catids) && $catids = preg_split('/[\s|,|;]/', $catids, -1, PREG_SPLIT_NO_EMPTY);

		if (empty($catids)) return;
		$categoryIds = ['in' => $catids];
		$collection = $this->_objectManager->get('Magento\Catalog\Model\Category')->getCollection();
		$collection->addAttributeToSelect('*')
			->setStoreId($this->_storeId)
			->addAttributeToFilter('entity_id', $categoryIds)
			->addIsActiveFilter();

		if ($orderby) {
			$attribute = $this->_getConfig('product_order_by','name');
			$dir = $this->_getConfig('product_order_dir','ASC');
			switch ($attribute) {
				case 'name':
				case 'position':
				case 'entry_id':
					$collection->addAttributeToSort($attribute, $dir);
					break;
				case 'random':
					$collection->getSelect()->order(new \Zend_Db_Expr('RAND()'));
					break;
				default:
			}
		}
		$_catids = [];

		if (empty($collection)) return;
		foreach ($collection as $category) {
			$_catids[] = $category->getId();
		}
		return $_catids;
	}

	private function _childCategory($catids, $allcat = true, $limitCat = 0, $levels = 0)
	{
		!is_array($catids) && settype($catids, 'array');
		$additional_catids = [];
		if (!empty($catids)) {

			foreach ($catids as $catid) {
				$_category = $this->_objectManager->get('Magento\Catalog\Model\Category')->load($catid);
				$levelCat = $_category->getLevel();
				if ($_category->hasChildren()){
					$catid_childs = $_category->getAllChildren(true);
					foreach ($catid_childs as $cat_child) {
						$_cat_child = $this->_objectManager->get('Magento\Catalog\Model\Category')->load($cat_child);
						$cat_child_level = $_cat_child->getLevel();
						$condition = ($cat_child_level - $levelCat <= $levels);
						if ($condition) {
							$additional_catids[] = $_cat_child->getId();
						}
					}
				}
			}
			$catids = $allcat ? array_unique(array_merge($catids, $additional_catids)) : array_unique($additional_catids);
		}
		return $catids;
	}

	public function _getProductsBasic($catids, $countProduct = false)
	{
		$collection = [];
		!is_array($catids) && settype($catids, 'array');
		if (!empty($catids)) {
			$collection = $this->_objectManager->get('Magento\Catalog\Model\Product')
				->getCollection()
				->addAttributeToSelect('*')
				->addAttributeToSelect('featured')
				->addMinimalPrice()
				->addFinalPrice()
				->addUrlRewrite()
				->setStoreId($this->_storeId)
				->joinField('category_id', 'catalog_category_product', 'category_id', 'product_id = entity_id', null, 'left')
				->addAttributeToFilter([['attribute' => 'category_id', 'in' => [$catids]]]);
			if ($this->_getFeaturedProduct($collection) == false) return null;
			$this->_getFeaturedProduct($collection);
			$collection->setVisibility($this->_objectManager->get('\Magento\Catalog\Model\Product\Visibility')->getVisibleInCatalogIds());
			$this->_addViewsCount($collection);
			$this->_addReviewsCount($collection);
			$collection->getSelect()->group('entity_id')->distinct(true);
			$this->_getOrder($collection);

			$collection->clear();
			if ($countProduct) return count($collection->getAllIds());
			$_start = 0;
			$_limit = (int)$this->_getConfig('product_limitation', 5);
			$_limit = $_limit <= 0 ? 0 : $_limit;
			if ($_limit >= 0) {
				$collection->getSelect()->limit($_limit, $_start);
			}
			$this->_objectManager->get('Magento\Review\Model\Review')->appendSummary($collection);
		}

		return $collection;
	}

	private function _getFeaturedProduct($collection)
	{
		$filter = (int)$this->_getConfig('product_featured', 0);
		$attributeModel = $this->_eavConfig->getAttribute('catalog_product', 'featured');
		switch ($filter) {
			// Show All
			case 0:
				break;
			// None Featured
			case 1:
				if ($attributeModel->usesSource()) {
					$collection->addAttributeToFilter([['attribute' => 'featured', 'eq' => 0]], null, 'left');
				}
				break;
			// Only Featured
			case 2:
				if ($attributeModel->usesSource()) {
					$collection->addAttributeToFilter([['attribute' => 'featured', 'eq' => 1]]);
				} else {
					return;
				}
				break;
		}
		return $collection;
	}

	// add views_count
	private function _addViewsCount(& $collection)
	{
		$reports_event_table = $this->_resource->getTableName('report_event');
		$select = $this->_resource->getConnection('core_read')
			->select()
			->from($reports_event_table, ['*', 'num_view_counts' => 'COUNT(`event_id`)'])
			->where('event_type_id = 1')
			->group('object_id');
		$collection->getSelect()
			->joinLeft(['mv' => $select],
				'mv.object_id = e.entity_id');
		return $collection;
	}

	// add reviews_count and rating_summary
	private function _addReviewsCount(& $collection)
	{
		$review_summary_table = $this->_resource->getTableName('review_entity_summary');
		$collection->getSelect()
			->joinLeft(
				["ra" => $review_summary_table],
				"e.entity_id = ra.entity_pk_value AND ra.store_id=" . $this->_storeId,
				[
					'num_reviews_count' => "ra.reviews_count",
					'num_rating_summary' => "ra.rating_summary"
				]
			);
		return $collection;
	}

	/*
		 *	Get Order
		 */
	private function _getOrder($collection)
	{
		$attribute = (string)$this->_getConfig('product_order_by', 'name');
		$dir = (string)$this->_getConfig('product_order_dir', 'ASC');
		switch ($attribute) {
			case 'entity_id':
			case 'name':
			case 'created_at':
				$collection->setOrder($attribute, $dir);
				break;
			case 'price':
				$collection->getSelect()->order('final_price ' . $dir . '');
				break;
			case 'random':
				$collection->getSelect()->order(new \Zend_Db_Expr('RAND()'));
				break;
			case 'lastest_product':
				$this->_getLastestProduct($collection);
				break;
			case 'top_rating':
				$collection->getSelect()->order('num_rating_summary ' . $dir . '');
				break;
			case 'most_reviewed':
				$collection->getSelect()->order('num_reviews_count ' . $dir . '');
				break;
			case 'most_viewed':
				$collection->getSelect()->order('num_view_counts ' . $dir . '');
				break;
			case 'best_sellers':
				$collection->getSelect()->order('ordered_qty ' . $dir . '');
				break;
			default:
		}
		return $collection;
	}

	/*
     *	Get Lastest Product
     */
	private function _getLastestProduct(& $collection)
	{
		$todayStartOfDayDate = $this->localeDate->date()->setTime(0, 0)->format('Y-m-d H:i:s');
		$todayEndOfDayDate = $this->localeDate->date()->setTime(23, 59, 59)->format('Y-m-d H:i:s');
		$collection = $this->_addProductAttributesAndPrices($collection)
			->addStoreFilter()
			->addAttributeToFilter('news_from_date',
				['or' => [
					0 => ['date' => true, 'to' => $todayEndOfDayDate],
					1 => ['is' => new \Zend_Db_Expr('null')]
				]], 'left')
			->addAttributeToFilter('news_to_date',
				['or' => [
					0 => ['date' => true, 'from' => $todayStartOfDayDate],
					1 => ['is' => new \Zend_Db_Expr('null')]
				]], 'left')
			->addAttributeToFilter([
				['attribute' => 'news_from_date', 'is' => new \Zend_Db_Expr('not null')],
				['attribute' => 'news_to_date', 'is' => new \Zend_Db_Expr('not null')],
			])
			->addAttributeToSort('news_from_date', 'DESC');
		return $collection;
	}

	public function _getProductsIDs()
	{
		$catids = $this->_getConfig('product_ids');
		!is_array($catids) && $catids = preg_split('/[\s|,|;]/', $catids, -1, PREG_SPLIT_NO_EMPTY);
		if (empty($catids)) return;
		$products = $this->_objectManager->get('Magento\Catalog\Model\Product')
			->getCollection()
			->addAttributeToSelect('*')
			->addIdFilter($catids);
		$products->getSelect()->order(new \Zend_Db_Expr('FIELD(e.entity_id, ' . implode(',', $catids) . ')'));
		return $products;
	}
}