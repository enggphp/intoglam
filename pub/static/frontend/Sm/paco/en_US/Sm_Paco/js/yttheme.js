define( [ "jquery" ], function ( $ ) {  
	$( document ).ready(function() {
		var w_window = $('.page-wrapper').innerWidth();
		$('.container-full').width($('.page-wrapper').innerWidth());
		$(window).resize(function(){
			$('.container-full').width($('.page-wrapper').width());
		});
	});
});

